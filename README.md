# README #

Nymz
Acronym / Initialism lookup utility
v1.0 (20170309)

### REQUIREMENTS ###

This project was written in Swift 3 for iOS 10.2 using Xcode 8.2.1.

### USAGE ###

* Select Acronym to enter an acronym or initialism (e.g., DOE). Select Full Name to enter a full name or description (e.g., Department of Energy).
* Click on the Clear (x) icon to clear the search bar.
* Tap Enter to submit the search.
* Select a Previous Search to view its result.
* Tap About to see details about this app, or link to the acronym API.