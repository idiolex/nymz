//
//  StringExtension.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/9/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import Foundation


// MARK: - EXTENSION -


extension String {
    
    // length
    var length : Int {
        
        return self.characters.count
        
    }
}
