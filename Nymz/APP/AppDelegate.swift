//
//  AppDelegate.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/8/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import UIKit


// MARK: - IMPLEMENTATION -


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    
    // MARK: - PROPERTIES -
    
    
    // main window
    var window: UIWindow?


    // MARK: - METHODS -
    
    
    //
    // launch
    //
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        return true
    }

    //
    // resign
    //
    func applicationWillResignActive(_ application: UIApplication) {
        
        // FUTURE
    }

    //
    // background
    //
    func applicationDidEnterBackground(_ application: UIApplication) {

        // FUTURE
    }

    //
    // foreground
    //
    func applicationWillEnterForeground(_ application: UIApplication) {

        // FUTURE
    }

    //
    // active
    //
    func applicationDidBecomeActive(_ application: UIApplication) {

        // FUTURE
    }

    //
    // terminate
    //
    func applicationWillTerminate(_ application: UIApplication) {

        // FUTURE
    }


}

