//
//  Search.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/8/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import Foundation


// MARK: - IMPLEMENTATION


class Search {
    
    
    // MARK: - PROPERTIES -
    
    
    // original search term
    var term = ""
    
    // long or short
    var mode = ""
    
    // main citation
    var organization : Organization?
    
    // variant citations
    var variants = [Organization]()
}
