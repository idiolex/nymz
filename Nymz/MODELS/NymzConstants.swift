//
//  NymzConstants.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/8/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import Foundation

import UIKit


// MARK: - ALIASES -


typealias StandardDictionary = [String : Any]


// MARK: - CONSTANTS


class NymzConstants {
    
    class tabs {
        
        static let short = 0
        static let long = 1
        static let shortPlaceholder = "Acronym Search"
        static let longPlaceholder = "Full Name Search"
    }
    
    class segues {
        
        static let details = "showDetailsView"
        static let about = "showAboutView"
    }
    
    class cells {
        
        static let searchList = "SearchListCell"
        static let detail = "DetailCell"
    }
    
    class details {
        
        static let all = [short, long, frequency, since]
        static let short = "short"
        static let long = "long"
        static let frequency = "frequency"
        static let since = "since"
    }
    
    class strings {
        
        static let noValue = "n/a"
    }

}
