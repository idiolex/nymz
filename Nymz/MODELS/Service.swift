//
//  Service.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/8/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import Foundation

import Alamofire


// MARK: - ALIASES -


typealias ServiceError = (title: String, message: String)


// MARK: - CONSTANTS -


class ServiceConstants {
    
    class endpoints {
        
        // details at http://www.nactem.ac.uk/software/acromine/
        static let acronym = "http://www.nactem.ac.uk/software/acromine/dictionary.py"
        static let url = URL(string: "http://www.nactem.ac.uk")!
    }
    
    class keys {
        
        static let shortForm = "sf"
        static let longForm = "lf"
        static let frequency = "freq"
        static let since = "since"
        static let options = "vars"
        static let longForms = "lfs"
        static let variants = "vars"
        
    }
}


// MARK: - IMPLEMENTATION -


class Service {
    
    // base class for service models
    
    //
    // generic request
    //
    class func request(url: URL, method: HTTPMethod, parameters: StandardDictionary, completion: @escaping (_ success: Bool, _ response: Any?)->Void) {
        
        // set up URLComponents from url
        var components = URLComponents(url: url, resolvingAgainstBaseURL: false)
        
        // add parameters
        if parameters.count > 0 {
            
            var queryItems = [URLQueryItem]()
            
            for parameter in parameters {
                
                let queryItem = URLQueryItem(name: parameter.key, value: parameter.value as? String)
                
                queryItems.append(queryItem)
            }
            
            components?.queryItems = queryItems

        }
     
        // make request
        Alamofire.request((components?.url)!, method: method, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseData { (response) in
            
            switch response.result {
                
            case .success(let responseData):
                
                do {
                    
                    // deserialize
                    let json = try JSONSerialization.jsonObject(with: responseData, options: []) as! [StandardDictionary]
                    
                    // callback with json
                    completion(true, json)
                    
                } catch {
                    
                    // response didn't deserialize
                    completion(false, ("WARNING", "Received response, but it was empty."))
                }
                
            case .failure(let error):
                
                // error with request or communications
                completion(false, ("REQUEST FAILED", "There was an error with the request: \(error.localizedDescription)"))
            }
        }
    }
}
