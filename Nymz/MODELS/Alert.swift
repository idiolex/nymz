//
//  Alert.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/9/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import Foundation

import UIKit


// MARK: - PROTOCOL -

protocol AlertDelegate {
    
}


// MARK: - CLASS -


class Alert {
    
    class func display(title: String, message: String, viewController: UIViewController? ) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        
        alert.addAction(okAction)
        
        viewController?.present(alert, animated: true, completion: nil)
        
    }
    
}
