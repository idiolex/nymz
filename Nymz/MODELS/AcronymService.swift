//
//  AcronymService.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/8/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import Foundation


// MARK: - IMPLEMENTATION -


class AcronymService : Service {
    
    // acronym service class
    
    
    // MARK: - PROPERTIES -
    
    
    // url for this service
    static let url = URL(string: ServiceConstants.endpoints.acronym)!
    
    
    // MARK: - METHODS -
    
    
    //
    // search
    //
    class func search(entry: String, mode: Int, completion: @escaping (_ success : Bool, _ response : Any?)->Void) {
        
        let key = mode == NymzConstants.tabs.short ? ServiceConstants.keys.shortForm : ServiceConstants.keys.longForm
        
        let parameters = [key : entry]
        
        AcronymService.request(url: url, method: .get, parameters: parameters) { (success, response) in
            
            completion(success, response)
        }
    }
}
