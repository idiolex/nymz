//
//  Organization.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/8/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import Foundation


// MARK: - IMPLEMENTATION -


class Organization {
    
    
    // MARK: - PROPERTIES -
    
    
    // acronym or initials
    var shortForm = ""
    
    // full name
    var longForm = ""
    
    // number of citations
    var frequency = 0
    
    // earliest citation
    var since = 0
    
    
    // MARK: - OBJECT METHODS -
    
    
    //
    // init w/ data
    //
    convenience init(shortForm sf: String, longForm lf: String, frequency freq: Int, since year: Int) {
        
        self.init()
        
        shortForm = sf
        longForm = lf
        frequency = freq
        since = year
    }
    
    //
    // item
    //
    func item(index: Int)->String {
        
        switch index {
            
        case 0: return shortForm
            
        case 1: return longForm
            
        case 2: return "\(frequency)"
            
        case 3: return "\(since)"
            
        default: return NymzConstants.strings.noValue
        
        }
    }
}
