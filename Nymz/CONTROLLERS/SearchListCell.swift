//
//  SearchListCell.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/8/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import Foundation

import UIKit


// MARK: - IMPLEMENTATION -


class SearchListCell : UITableViewCell {
    
    
    // MARK: - PROPERTIES -
    
    
    // label
    @IBOutlet var label : UILabel?
}

