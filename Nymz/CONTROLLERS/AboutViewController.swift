//
//  AboutViewController.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/8/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import Foundation

import UIKit


// MARK: - IMPLEMENTATION -


class AboutViewController : NymzViewController {
    
    
    // MARK: - PROPERTIES -
    
    
    // view model
    var viewModel : AboutViewModel?
    
    // OUTLETS
    
    // icon
    @IBOutlet var logoImageView : UIImageView?
    @IBOutlet var logoShadow : UIImageView?
    
    // idiolex logo
    @IBOutlet var idiolexImageView : UIImageView?
    @IBOutlet var idiolexShadow : UIImageView?
    
    // app details
    @IBOutlet var titleLabel : UILabel?
    @IBOutlet var subtitleLabel : UILabel?
    @IBOutlet var versionLabel : UILabel?
    
    // attribution
    @IBOutlet var linkLabel : UILabel?
    
    
    // MARK: - OBJECT METHOD -
    
    
    // 
    // load
    //
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        viewModel = AboutViewModel(viewController: self)
        
        viewModel?.setup()
    }
    
    //
    // appear
    //
    override func viewWillAppear(_ animated: Bool) {
        
        viewModel?.appear()
    }
    
    //
    // disappear
    //
    override func viewWillDisappear(_ animated: Bool) {
        
        viewModel?.disappear()
    }
}
