//
//  SearchListViewController.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/8/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import Foundation

import UIKit


// MARK: - IMPLEMENTATION -


class SearchListViewController : NymzTableViewController {
    
    
    // MARK: - PROPERTIES -
    
    
    // view model
    var viewModel : SearchListViewModel?
    
    // OUTLETS
    
    // searchbar
    @IBOutlet var searchBar : UISearchBar?
    
    // tabs
    @IBOutlet var shortLabel : UILabel?
    @IBOutlet var longLabel : UILabel?
    
    // underline
    @IBOutlet var underlineView : UIView?
    
    // spinner
    @IBOutlet var spinner : UIActivityIndicatorView?
    
    // MARK: - OBJECT METHODS -
    
    
    //
    // load
    //
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        viewModel = SearchListViewModel(viewController: self)
        
        viewModel?.setup()
    }
    
    //
    // appear
    //
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        viewModel?.appear()
    }
    
    //
    // disappear
    //
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        viewModel?.disappear()
    }
    
    
    // MARK: - TABLE METHODS -
    
    
    //
    // sections
    //
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return (viewModel?.sections())!
    }
    
    //
    // rows
    //
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (viewModel?.rows(section: section))!
    }
    
    //
    // cell
    //
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return (viewModel?.cell(indexPath: indexPath))!
    }
    
    // 
    // select
    //
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        viewModel?.select(indexPath: indexPath)
    }
    
    // 
    // prepare
    //
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        viewModel?.prepare(segue: segue, sender: sender)
    }
}
