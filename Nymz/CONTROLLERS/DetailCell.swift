//
//  DetailCell.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/9/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import Foundation

import UIKit


// MARK: - IMPLEMENTATION -


class DetailCell : UITableViewCell {
    
    
    // MARK: - PROPERTIES -
    
    
    // label
    @IBOutlet var label : UILabel?
    
    // value
    @IBOutlet var value : UILabel?
}
