//
//  SearchDetailsViewController.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/8/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import Foundation

import UIKit


// MARK: - IMPLEMENTATION -


class SearchDetailsViewController : NymzTableViewController {
    
    
    // MARK: - PROPERTIES -
    
    
    // view model
    var viewModel : SearchDetailsViewModel?
    
    
    // MARK: - OBJECT METHODS -
    
    
    //
    // load
    //
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        if viewModel == nil {
        
            viewModel = SearchDetailsViewModel(viewController: self)
        
        }
        
        viewModel?.setup()
    }
    
    //
    // appear
    //
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        viewModel?.appear()
    }
    
    
    // MARK: - TABLE METHODS -
    
    
    //
    // sections
    //
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return (viewModel?.sections())!
    }
    
    // 
    // rows
    //
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return (viewModel?.rows(section: section))!
    }
    
    //
    // cell
    //
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return (viewModel?.cell(indexPath: indexPath))!
    }
    
    // 
    // header 
    //
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return viewModel?.header(section: section)
    }

}
