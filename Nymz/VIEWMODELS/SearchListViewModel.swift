//
//  SearchListViewModel.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/8/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import Foundation

import UIKit


// MARK: - IMPLEMENTATION -


class SearchListViewModel : NSObject,
    UISearchBarDelegate {
    
    
    // MARK: - PROPERTIES -
    
    
    // view controller
    var viewController : SearchListViewController?
    
    // function tabs
    var tabs = [UILabel]()
    
    // search history
    var searches = [Search]()
    
    // search mode
    var _mode = 0
    
    var mode : Int {
        
        get { return _mode }
        
        set {
            
            _mode = newValue
            
            // update ui w/ new value
            highlight(index: newValue)
        }
    }
    
    // string title of mode
    var modeString : String {
        
        return mode == NymzConstants.tabs.short ? ServiceConstants.keys.shortForm : ServiceConstants.keys.longForm
    }
    
    
    // MARK: - OBJECT METHODS - 
    
    
    //
    // init w/ data
    //
    convenience init(viewController searchListViewController : SearchListViewController) {
        
        self.init()
        
        viewController = searchListViewController
        
    }
    
    //
    // setup
    //
    func setup() {
        
        // gesture recognizer
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:)))
        
        recognizer.numberOfTapsRequired = 1
        
        viewController?.view.addGestureRecognizer(recognizer)
        
        // tabs array
        tabs.append((viewController?.shortLabel)!)
        tabs.append((viewController?.longLabel)!)

        // set searchbar delegate
        viewController?.searchBar?.delegate = self

    }
    
    //
    // appear
    //
    func appear() {
        
        // update list
        viewController?.tableView.reloadData()
    }
    
    //
    // disappear
    //
    func disappear() {
        
        // remove gesture recognizers
        viewController?.view.gestureRecognizers?.removeAll()
    }
    
    
    // MARK: - TAB METHODS -
    
    
    //
    // handle gesture
    //
    func handleTap(recognizer : UITapGestureRecognizer) {
        
        // find tab
        let point = recognizer.location(in: viewController?.view)
        
        for (index, tab) in tabs.enumerated() {
            
            if tab.frame.contains(point) {
                
                mode = index
                
                break
            }
        }
        
    }
    
    //
    // highlight
    //
    func highlight(index: Int) {
        
        // get tab
        let tab = tabs[index]
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.6, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
            
            // move underline
            self.viewController?.underlineView?.frame.origin = CGPoint(x: tab.frame.origin.x, y: (self.viewController?.underlineView?.frame.origin.y)!)
        
            // update text colors
            self.viewController?.shortLabel?.textColor = index == NymzConstants.tabs.short ? UIColor.black : UIColor.darkGray
            
            self.viewController?.longLabel?.textColor = index == NymzConstants.tabs.long ? UIColor.black : UIColor.darkGray
            
            self.viewController?.searchBar?.placeholder = index == NymzConstants.tabs.short ? NymzConstants.tabs.shortPlaceholder : NymzConstants.tabs.longPlaceholder
            
        }) { (finished) in
            
            // FUTURE
        }
    }
    
    
    // MARK: - TABLE METHODS -
    
    
    //
    // sections
    //
    func sections()->Int {
        
        return 1
    }
    
    //
    //rows
    //
    func rows(section: Int)->Int {
        
        return searches.count
    }
    
    //
    // cell
    //
    func cell(indexPath: IndexPath)->SearchListCell {
        
        // dequeue cell
        let cell = viewController?.tableView.dequeueReusableCell(withIdentifier: NymzConstants.cells.searchList, for: indexPath) as! SearchListCell
        
        // update w/ search data
        let search = searches[indexPath.row]
        
        cell.label?.text = "\(search.mode) = \(search.term)"

        return cell
    }
    
    //
    // select
    //
    func select(indexPath: IndexPath) {
        
        // show search details
        let search = searches[indexPath.row]

        viewController?.performSegue(withIdentifier: NymzConstants.segues.details, sender: search)
    }
    
    
    // MARK: - SEARCH METHODS -
    
    
    //
    // start search
    //
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        self.viewController?.spinner?.startAnimating()
        
        search(term: searchBar.text, mode: mode)
    }
    
    //
    // cancel search
    //
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        searchBar.text = ""
    }
    
    //
    // search
    //
    func search(term: String?, mode: Int) {
        
        // validate
        if (term != nil) && (term?.length)! > 0 {
            
            // request
            AcronymService.search(entry: term!, mode: mode, completion: { (success, response) in
                
                if success {
                    
                    // process response
                    let dict = response as! [StandardDictionary]
                    
                    let search = self.search(response: dict)
                    
                    search.term = term!
                    search.mode = self.modeString
                    
                    // add to list
                    self.searches.append(search)
                    
                    // show details
                    self.viewController?.performSegue(withIdentifier: NymzConstants.segues.details, sender: search)
                    
                } else {
                    
                    // show error
                    let serviceError = response as! ServiceError
                    
                    Alert.display(title: serviceError.title, message: serviceError.message, viewController: self.viewController)
                }
                
                self.viewController?.spinner?.stopAnimating()
            })
            
        }
        
    }
    
    //
    // process response
    //
    func search(response: [StandardDictionary])->Search {
        
        let result = Search()
        
        // get dict from array
        if let dict = response.first {
            
            var organizations = [Organization]()
            
            // get main org deets
            let sf = dict[ServiceConstants.keys.shortForm] as? String ?? "n/a"
            
            let lfs = dict[ServiceConstants.keys.longForms] as? [StandardDictionary] ?? [StandardDictionary]()
            
            let mainDict = lfs.first
            
            let lf = mainDict?[ServiceConstants.keys.longForm] as? String ?? "n/a"
            
            let freq = mainDict?[ServiceConstants.keys.frequency] as? Int ?? 0
            
            let since = mainDict?[ServiceConstants.keys.since] as? Int ?? 0
            
            var mainOrganization = Organization(shortForm: sf, longForm: lf, frequency: freq, since: since)

            // get variants
            let vars = mainDict?[ServiceConstants.keys.variants] as? [StandardDictionary] ?? [StandardDictionary]()
            
            for organizationDict in vars {
                
                let lf = organizationDict[ServiceConstants.keys.longForm] as? String ?? "n/a"
                
                let freq = organizationDict[ServiceConstants.keys.frequency] as? Int ?? 0
                
                let since = organizationDict[ServiceConstants.keys.since] as? Int ?? 0
                
                let organization = Organization(shortForm: sf, longForm: lf, frequency: freq, since: since)
                
                // check for most frequent
                if freq > mainOrganization.frequency {
                    
                    // add former leader to list
                    organizations.append(mainOrganization)
                    
                    // update leader
                    mainOrganization = organization
                
                } else {
                    
                    // add to list
                    organizations.append(organization)
                    
                }
            }
            
            // update result
            result.organization = mainOrganization
            result.variants = organizations
        }
        
        return result
    }
    
    
    // MARK: - MISC -
    
    
    //
    // segue
    //
    func prepare(segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier! {
            
        case NymzConstants.segues.details:
            
            // set up details view controller
            let detailsViewController = segue.destination as? SearchDetailsViewController
            
            let detailsViewModel = SearchDetailsViewModel(viewController: detailsViewController!)
            
            // inject data
            detailsViewModel.search = sender as? Search
            
            detailsViewController?.viewModel = detailsViewModel
            
        default:
            
            break
        }
    }
}
