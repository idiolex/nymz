//
//  AboutViewModel.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/8/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import Foundation

import UIKit


// MARK: - IMPLEMENTATION -


class AboutViewModel : NSObject {
    
    
    // MARK: - PROPERTIES -
    
    
    // view controller
    var viewController : AboutViewController?
    
   
    // MARK: - OBJECT METHODS -
    
    
    //
    // init w/ data
    //
    convenience init(viewController aboutViewController : AboutViewController) {
        
        self.init()
        
        viewController = aboutViewController
    }
    
    //
    // setup
    //
    func setup() {
        
        // add gesture recognizer
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(recognizer:)))
        
        recognizer.numberOfTapsRequired = 1
        
        viewController?.view.addGestureRecognizer(recognizer)
        
    }
    
    
    //
    // appear
    //
    func appear() {
        
        // icon
        viewController?.logoImageView?.layer.cornerRadius = 25.0
        viewController?.logoImageView?.layer.masksToBounds = true
        
        // shadow
        viewController?.logoShadow?.layer.shadowColor = UIColor.black.cgColor
        viewController?.logoShadow?.layer.shadowRadius = 18.0
        viewController?.logoShadow?.layer.shadowOpacity = 0.75
        viewController?.logoShadow?.layer.shadowOffset = CGSize(width: 12.0, height: 12.0)
        viewController?.logoShadow?.layer.masksToBounds = false
        
        // logo
        viewController?.idiolexImageView?.layer.cornerRadius = 5.0
        viewController?.idiolexImageView?.layer.masksToBounds = true
        
        // details
        
        let versionString = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
        
        let buildString = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion")as! String
        
        viewController?.versionLabel?.text = "Version \(versionString) [ build \(buildString) ]"
        
    }
    
    //
    // disappear
    //
    func disappear() {
        
        viewController?.view.gestureRecognizers?.removeAll()
    }

    
    // MARK: - MISC -
    
    
    //
    // handle tap
    //
    func handleTap(recognizer : UITapGestureRecognizer) {
        
        
        // check if url tapped
        let point = recognizer.location(in: viewController?.view)
        
        // show url
        if (viewController?.linkLabel?.frame.contains(point))! {
            
            show(url: ServiceConstants.endpoints.url)
        }
    }
    
    //
    // show url
    //
    func show(url: URL) {
        
        // check ios version
        if #available(iOS 10.0, *) {

            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        
        } else {
        
            UIApplication.shared.openURL(url)
        
        }
    }
    }
