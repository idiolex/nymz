//
//  SearchDetailsViewModel.swift
//  Nymz
//
//  Created by Adrian Mannella on 3/8/17.
//  Copyright © 2017 Idiolex Software. All rights reserved.
//


// MARK: - HEADERS -


import Foundation

import UIKit


// MARK: - IMPLEMENTATION -


class SearchDetailsViewModel : NSObject {
    
    
    // MARK: - PROPERTIES -
    
    
    // view controller
    var viewController : SearchDetailsViewController?
    
    // search data
    var search : Search?
    
    
    // MARK: - OBJECT METHODS -
    
    
    //
    // init w/ data
    //
    convenience init(viewController searchDetailsViewController : SearchDetailsViewController) {
        
        self.init()
        
        viewController = searchDetailsViewController
    }
    
    //
    // setup
    //
    func setup() {
        
        // FUTURE
    }
    
    //
    // appear
    //
    func appear() {
        
        // FUTURE
    }
    
    
    // MARK: - TABLE METHODS -
    
    
    //
    // sections
    //
    func sections()->Int {
        
        // count of variants plus one for main citation
        return search == nil ? 0 : search!.variants.count + 1
    }
    
    //
    // header 
    //
    func header(section: Int)->String {
        
        switch section {
        
        case 0:
            
            // ex: sf = <acronym>, or lf = <full name>
            return "Search (\(search?.mode ?? "?") = \(search?.term ?? "n/a"))"
            
        default:
            
            // n of m
            return "Variant \(section) / \(search?.variants.count ?? 0)"
        }
    }
    
    //
    // rows
    //
    func rows(section: Int)->Int {
        
        return 4
    }
    
    // 
    // cell
    //
    func cell(indexPath : IndexPath)->DetailCell {
        
        // dequeue cell
        let cell = viewController?.tableView.dequeueReusableCell(withIdentifier: NymzConstants.cells.detail, for: indexPath) as! DetailCell
        
        // populate w/ data
        let organization = indexPath.section == 0 ? search?.organization : search?.variants[indexPath.section - 1]
        
        cell.label?.text = NymzConstants.details.all[indexPath.row]
        
        cell.value?.text = organization?.item(index: indexPath.row) ?? NymzConstants.strings.noValue
        
        return cell
        
    }
}
